package com.backroomsquad.piwomin.Entry;

/**
 * An Entry for a beer from an SQL database 
 * 
 * @author Jonas Birkedal Dudal Jensen
 *
 */
public class Entry {
	private final String name;
	private final String brewery;
	private final String type;
	private final double price;
	private final double strength;
	private final String supermarket;
	private final String address;
	
	public Entry(String name, String brewery, String type, double price, double strength, String supermarket, String address) {
		this.name = name;
		this.brewery = brewery;
		this.type = type;
		this.price = price;
		this.strength = strength;
		this.supermarket = supermarket;
		this.address = address;
	}

	public String getName() {
		return name;
	}

	public String getBrewery() {
		return brewery;
	}

	public String getType() {
		return type;
	}

	public double getPrice() {
		return price;
	}

	public double getStrength() {
		return strength;
	}

	public String getPricePretty() {
		return price + (((int) (price*10) == price*10) ? "0" : "");
	}

	public String getSupermarket() {
		return supermarket;
	}

	public String getAddress() {
		return address;
	}

	@Override
	public String toString() {
		return "Name: " + getName() + ", Brewery: " + getBrewery() + ", Type: " + getType() + ", Price: " + getPrice() + "zł, Strength: " + getStrength() + "%, Supermarket: " + getSupermarket() + ", Address: " + getAddress();
	}
}
