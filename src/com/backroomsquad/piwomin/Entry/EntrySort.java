package com.backroomsquad.piwomin.Entry;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * A simple class that sorts a list of entries
 * @author Jonas Birkedal Dudal Jensen
 *
 */

public class EntrySort {
	/**
	 * sort is a method that sorts a list of unsorted entries by a filter
	 * 
	 * @param entries - a list of unsorted entries
	 * @param sort - a filter to sort the list by
	 * @param smallestToLargest - a boolean that tells sort() if it should sort smallest to largest or largest to smallest
	 * @return returns a list of entries sorted by the sort filter
	 */
	public static List<Entry> sort(List<Entry> entries, Filters sort, boolean smallestToLargest) {
		Collections.sort(entries, new Comparator<Entry>() {
			@Override
			public int compare(Entry e0, Entry e1) {
				if (sort.equals(Filters.NAME)) return e0.getName().compareTo(e1.getName());
				if (sort.equals(Filters.BREWERY)) return e0.getBrewery().compareTo(e1.getBrewery());
				if (sort.equals(Filters.TYPE)) return e0.getType().compareTo(e1.getType());
				if (sort.equals(Filters.PRICE)) return (int) ((e0.getPrice() - e1.getPrice())*1000);
				if (sort.equals(Filters.STRENGTH)) return (int) ((e0.getStrength() - e1.getStrength())*1000);
				if (sort.equals(Filters.SUPERMARKET)) return e0.getSupermarket().compareTo(e1.getSupermarket());
				if (sort.equals(Filters.ADDRESS)) return e0.getAddress().compareTo(e1.getAddress());
				
				return 0;
			}
		});
		
		if (!smallestToLargest) Collections.reverse(entries);
		
		return entries;
	}
}
