package com.backroomsquad.piwomin.Entry;

/**
 * A list of filters used to filter entries
 * 
 * @author Jonas Birkedal Dudal Jensen
 *
 */

public enum Filters {
	NAME("Beer.Name"), BREWERY("Beer.Brewery"), TYPE("Beer.Type"), PRICE("Beer.Price"), STRENGTH("Beer.Strength"), SUPERMARKET("Supermarket.Name"), ADDRESS("Supermarket.Address");
	String tag;
	Filters(String tag) {
		this.tag = tag;
	}
	
	public String getTag() {
		return tag;
	}
}