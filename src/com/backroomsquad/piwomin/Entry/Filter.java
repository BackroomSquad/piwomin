package com.backroomsquad.piwomin.Entry;

/**
 * A class used to make a simple filter, to filter entities
 * 
 * @author Jonas Birkedal Dudal Jensen
 *
 */
public class Filter {
	private Filters filter;
	private Object value;
	
	public Filter(Filters filter, double[] value) {
		this.filter = filter;
		this.value = value;
	}
	
	public Filter(Filters filter, String value) {
		this.filter = filter;
		this.value = value;
	}

	public Filters getFilter() {
		return filter;
	}

	public Object getValue() {
		return value;
	}
}
