package com.backroomsquad.piwomin.SQL;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

import com.backroomsquad.piwomin.Entry.Entry;
import com.backroomsquad.piwomin.Entry.Filter;

/**
 * A simple handler for an SQLite database
 * 
 * @author Jonas Birkedal Dudal Jensen
 *
 */
public class SQL {
	
	/**
	 * Returns data from an SQL database
	 * 
	 * <p>
	 * loadSQL returns a list of all entries that falls within given filters.
	 * If strings are passed through the filters, then a list of entries that match the values will be passed.
	 * If double[]'s are passed, then ranges are created, with index 0 as the lower bounds and index 1 as the higher, a list of entries is then passed that falls within the ranges.
	 * The String and double[] filters can be combined at will and will work like an AND if done so
	 * </p>
	 * 
	 * @param filters - a filter that describes a set of parameters to filter 
	 * @return a list of entries that matches the given filter
	 */
	public static List<Entry> loadSQL(Filter... filters) {
		//Creates a list of beer entries
		List<Entry> entries = new ArrayList<Entry>();
		
		try {
			//Creates a connection to the SQL table with SQLite
			Connection c = DriverManager.getConnection("jdbc:sqlite::resource:SQL/Table.db");
			
			ResultSet rs = null;
			
			//String that contains the SQL instruction
			//This is done so we can have more than one filter applied at once
			String sql = "SELECT Beer.Name, Beer.Brewery, Beer.Type, Beer.Price, Beer.Strength, Supermarket.Name, Supermarket.Address FROM Beer INNER JOIN Supermarket ON Beer.Supermarket_ID=Supermarket.ID WHERE ";
			
			//Goes through all the filters
			for (Filter filter : filters) {
				if (filter.getValue() instanceof String) {
					//If value is empty, then don't add it to the query
					if (((String) filter.getValue()).isEmpty()) continue;
					
					//SELECTS everything FROM the table called Beer, if the column name specified by the filter contains the value
					sql += filter.getFilter().getTag() + " LIKE ? ";
				} else if (filter.getValue() instanceof double[]) {
					//If both values are -1, then don't add it to the query
					if (((double[]) filter.getValue())[0] == -1 || ((double[]) filter.getValue())[1] == -1) continue;
					
					//SELECTS everything FROM a table called Beer, if the column name specified by the filter is BETWEEN value[0] and value[1]	
					sql += filter.getFilter().getTag() + " BETWEEN ? AND ? ";
				}
				
				sql += "AND ";
			}
			
			sql = sql.substring(0, sql.substring(0, sql.length()-1).lastIndexOf(' '));
			
			//Creates the prepared statement
			PreparedStatement ps = c.prepareStatement(sql);
			
			int filterIndex = 1;
			for (int i = 0; i < filters.length; i++) {
				Filter filter = filters[i];
				
				if (filter.getValue() instanceof String) {
					if (((String) filter.getValue()).isEmpty()) continue;
					
					//% is used so it matches anything that contains value
					ps.setString(filterIndex++, "%" + (String) filter.getValue() + "%");
				} else if (filter.getValue() instanceof double[]) {
					if (((double[]) filter.getValue())[0] == -1 || ((double[]) filter.getValue())[1] == -1) continue;
					
					ps.setDouble(filterIndex++, ((double[]) filter.getValue())[0]);
					ps.setDouble(filterIndex++, ((double[]) filter.getValue())[1]);
				}
			}

			rs = ps.executeQuery();

			//Goes through all elements and adds them to a list of entries
			while (rs.next()) {
				String name = rs.getString(1);
				String brewery = rs.getString(2);
				String type = rs.getString(3);
				double price = rs.getDouble(4);
				double strength = rs.getDouble(5);
				String supermarket = rs.getString(6);
				String address = rs.getString(7);
				
				entries.add(new Entry(name, brewery, type, price, strength, supermarket, address));
			}
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
		
		return entries;
	}
}
	