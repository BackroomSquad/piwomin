package com.backroomsquad.piwomin.GUI;

import java.awt.Dimension;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeListener;

import javax.swing.*;

import com.backroomsquad.piwomin.Entry.Filter;
import com.backroomsquad.piwomin.Entry.Filters;
import com.backroomsquad.piwomin.SQL.SQL;

/**
 * Handles all GUI elements
 * 
 * @author Lene H�jberg Christensen
 *
 */
public class GUI {
	public Table table;
	
	/**<p>
	 * Adds all GUI elements to frame except for table
	 * </p>
	 * 
	 * @param frame The frame that shows elements
	 */
	public GUI(JFrame frame) {
		//Adds textfield and label for the name of the beer
		JTextField name = new JTextField();
		name.setBounds(frame.getWidth()/14, frame.getHeight()/6, frame.getWidth()/8, 20);	
		frame.add(name);
		JLabel namel = new JLabel("Name");
		namel.setHorizontalAlignment(JLabel.CENTER);
		namel.setBounds(frame.getWidth()/14, frame.getHeight()/10, frame.getWidth()/8, 20);
		frame.add(namel);
		
		//Adds the textfield and label for the name of the brewery
		JTextField brew = new JTextField();
		brew.setBounds((frame.getWidth()/14)*3, frame.getHeight()/6, frame.getWidth()/8, 20);	
		frame.add(brew);
		JLabel brewl = new JLabel("Brewery");
		brewl.setHorizontalAlignment(JLabel.CENTER);
		brewl.setBounds((frame.getWidth()/14)*3, frame.getHeight()/10, frame.getWidth()/8, 20);
		frame.add(brewl);
		System.out.println(frame.getWidth());
		
		//Adds the textfield and label for the type
		JTextField type = new JTextField();
		type.setBounds((frame.getWidth()/14)*5, frame.getHeight()/6, frame.getWidth()/8, 20);	
		frame.add(type);
		JLabel typel = new JLabel("Type");
		typel.setHorizontalAlignment(JLabel.CENTER);
		typel.setBounds((frame.getWidth()/14)*5, frame.getHeight()/10, frame.getWidth()/8, 20);
		frame.add(typel);
		
		//Adds textfields and label for the price range
		JTextField pricelow = new 	JTextField();
		pricelow.setBounds((frame.getWidth()/14)*7, frame.getHeight()/6, frame.getWidth()/16+2, 20);	
		frame.add(pricelow);
		JTextField pricehigh = new 	JTextField();
		pricehigh.setBounds((int) ((frame.getWidth()/14)*8)-2, frame.getHeight()/6, frame.getWidth()/16, 20);	
		frame.add(pricehigh);
		JLabel pricel = new JLabel("Price");
		pricel.setHorizontalAlignment(JLabel.CENTER);
		pricel.setBounds((int) ((frame.getWidth()/14)*7), frame.getHeight()/10, frame.getWidth()/8, 20);
		frame.add(pricel);
		
		//Adds textfields and label for the strength range
		JTextField strengthlow = new JTextField();
		strengthlow.setBounds((int) ((frame.getWidth()/14)*9), frame.getHeight()/6, frame.getWidth()/16+2, 20);	
		frame.add(strengthlow);
		JTextField strengthhigh = new JTextField();
		strengthhigh.setBounds((int) ((frame.getWidth()/14)*10)-2, frame.getHeight()/6, frame.getWidth()/16, 20);	
		frame.add(strengthhigh);
		JLabel strengthl = new JLabel("Strength");
		strengthl.setHorizontalAlignment(JLabel.CENTER);
		strengthl.setBounds((int) ((frame.getWidth()/14)*9), frame.getHeight()/10, frame.getWidth()/8, 20);
		frame.add(strengthl);
		
		//Adds the textfield and label for the name of the supermarket
		JTextField supermarket = new JTextField();
		supermarket.setBounds((frame.getWidth()/14)*11, frame.getHeight()/6, frame.getWidth()/8, 20);	
		frame.add(supermarket);
		JLabel supermarketl = new JLabel("Supermarket");
		supermarketl.setHorizontalAlignment(JLabel.CENTER);
		supermarketl.setBounds((frame.getWidth()/14)*11, frame.getHeight()/10, frame.getWidth()/8, 20);
		frame.add(supermarketl);
		
		table = new Table();
		JPanel panel = new JPanel();
		panel.setBounds(frame.getWidth()/14, frame.getHeight()/10*3, frame.getWidth()/14*12, frame.getHeight()/10*6);
		JScrollPane scroll = new JScrollPane(table);
		scroll.setPreferredSize(panel.getSize());
		panel.add(scroll);
		frame.add(panel);
		
		JButton search = new JButton("Search");
		search.setBounds(((frame.getWidth()/8*7)-5), (int)((frame.getHeight()/10)*2.5), frame.getWidth()/9, 20);
		search.setMargin(new Insets(0,0,0,0));
		search.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				getData(name.getText(), brew.getText(), type.getText(), pricehigh.getText().isEmpty()?-1:Double.parseDouble(pricehigh.getText()), pricelow.getText().isEmpty()?-1:Double.parseDouble(pricelow.getText()), strengthhigh.getText().isEmpty()?-1:Double.parseDouble(strengthhigh.getText()), strengthlow.getText().isEmpty()?-1:Double.parseDouble(strengthlow.getText()), supermarket.getText());
			}
		});
		frame.add(search);
		
	}

	public void render(JFrame frame) {
		
	}
	
	public void getData(String name, String brew, String type, double priceHigh, double priceLow, double strengthHigh, double strengthLow, String supermarket) {
		table.dataDump(SQL.loadSQL(new Filter(Filters.NAME, name), new Filter(Filters.BREWERY, brew), new Filter(Filters.TYPE,type), new Filter(Filters.PRICE, new double[] {priceLow, priceHigh}), new Filter(Filters.STRENGTH, new double[] {strengthLow, strengthHigh}), new Filter(Filters.SUPERMARKET, supermarket)));
	}
}
