package com.backroomsquad.piwomin.GUI;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.List;

import javax.swing.JTable;
import javax.swing.event.TableModelListener;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;

import com.backroomsquad.piwomin.Entry.Entry;
import com.backroomsquad.piwomin.Entry.EntrySort;
import com.backroomsquad.piwomin.Entry.Filters;

/**
 * Takes data from com.backroomsquad.piwomin.SQL.SQL and adds them to table
 * 
 * @author Lene Højberg Christensen
 *
 */
public class Table extends JTable {
	private String[] originalColumnName = {"Name", "Brewery", "Type", "Price", "Strength", "Supermarket", "Address"};
	private String[] columnName;
	private Object[][] data;
	private List<Entry> entries;
	private int lastCol = -1;
	
	public Table() {
		columnName = originalColumnName;
		
		//Adds a listener to table header so it can sort the entries when the specific column is clicked
		getTableHeader().addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent evt) {
				//Converts the given point of the mouse to a column
				int col = columnAtPoint(evt.getPoint());
				
				//Adds arrow based on if the sorting is incremental or decremental
				columnName = originalColumnName.clone();
				columnName[col] += " " + ((lastCol == -1) ? "▾" : "▴");
				
				//Sorts the entries and updates the table with the newly sorted list
				dataDump(EntrySort.sort(entries, Filters.values()[col], lastCol != col));
				//if lastCol is not equal to col, then either the user clicked on a new column or the last time the column was sorted, it was sorted largest to smallest
				if (lastCol != col) lastCol = col;
				else lastCol = -1;
			}
		});
	}
	
	public void dataDump(List<Entry> entries) {
		this.entries = entries;
		
		data = new Object[entries.size()][columnName.length];
		
		for (int i = 0; i < entries.size(); i++) {
			Entry e = entries.get(i);
			
			data[i] = new Object[]{e.getName(), e.getBrewery(), e.getType(), e.getPricePretty() + " zł", e.getStrength() + "%", e.getSupermarket(), e.getAddress()};
		}
		
		setModel(new DefaultTableModel(data, columnName));
	}
}
