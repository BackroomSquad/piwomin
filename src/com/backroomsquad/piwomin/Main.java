package com.backroomsquad.piwomin;

import java.awt.Dimension;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextField;
import com.backroomsquad.piwomin.GUI.GUI;

public class Main implements Runnable {
	private static Thread mainThread;
	public static int SCREEN_WIDTH = 640, SCREEN_HEIGHT = 480;
	private static JFrame frame;
	private GUI gui;
	
	public Main() {
		frame = new JFrame("PiwoMin");
		frame.setSize(new Dimension(SCREEN_WIDTH, SCREEN_HEIGHT));
		gui = new GUI(frame);
	}
	
	private void start() {
		mainThread = new Thread(this);
		mainThread.start();
	}
	
	public static void main(String[] args) {
		Main main = new Main();
		
		frame.setLocationRelativeTo(null);
		frame.setLayout(null);
		frame.setResizable(false);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setVisible(true);
		//Sets up the screen
		
		//Starts all processes
		main.start();
	}

	@Override
	public void run() {
		while (true) {
			gui.render(frame);
		}
	}
}
